package br.com.senac.calculadora;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {


    private static final char ADICAO = '+' ;
    private static final char SUBTRACAO = '-' ;
    private static final char MULTIPLICACAO = '*' ;
    private static final char DIVISAO = '/' ;


    private char operacao ;
    private double operando1 = Double.NaN;
    private double operando2;



    private boolean limpaDisplay = true ;

    private Display display ;
    private ClickDigito clickDigito  ;


    private TextView txtDisplay ;
    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;
    private Button btn6;
    private Button btn7;
    private Button btn8;
    private Button btn9;
    private Button btn0;
    private Button btnPonto ;
    private Button btnLimpar ;
    private Button btnSoma ;
    private Button btnSubtracao ;
    private Button btnMultiplicacao ;
    private Button btnDivisao ;
    private Button btnIgual ;

    private void init(){
      /*
        int orientacao  =  this.getResources().getConfiguration().orientation ;


      if(orientacao == Configuration.ORIENTATION_PORTRAIT){
          Toast.makeText(this,"Orientacao Portrait" , Toast.LENGTH_LONG).show();
      }else{
          Toast.makeText(this,"Orientacao Landscape" , Toast.LENGTH_LONG).show();
      }
*/


        this.txtDisplay = findViewById(R.id.display);
        this.btn1 = findViewById(R.id.btn1);
        this.btn2 = findViewById(R.id.btn2);
        this.btn3 = findViewById(R.id.btn3);
        this.btn4 = findViewById(R.id.btn4);
        this.btn5 = findViewById(R.id.btn5);
        this.btn6 = findViewById(R.id.btn6);
        this.btn7 = findViewById(R.id.btn7);
        this.btn8 = findViewById(R.id.btn8);
        this.btn9 = findViewById(R.id.btn9);
        this.btn0 = findViewById(R.id.btn0);
        this.btnPonto = findViewById(R.id.btnPonto);
        this.btnLimpar = findViewById(R.id.btnLimpar);
        this.btnIgual  = findViewById(R.id.btnIgual);

        this.btnSoma  = findViewById(R.id.btnSoma);
        this.btnSubtracao  = findViewById(R.id.btnSubtracao);
        this.btnMultiplicacao  = findViewById(R.id.btnMultiplicacao);
        this.btnDivisao = findViewById(R.id.btnDivisao);

        this.display = new Display(txtDisplay);

        this.clickDigito = new ClickDigito(this.display);

        this.btn1.setOnClickListener(clickDigito);
        this.btn2.setOnClickListener(clickDigito);
        this.btn3.setOnClickListener(clickDigito);
        this.btn4.setOnClickListener(clickDigito);
        this.btn5.setOnClickListener(clickDigito);
        this.btn6.setOnClickListener(clickDigito);
        this.btn7.setOnClickListener(clickDigito);
        this.btn8.setOnClickListener(clickDigito);
        this.btn9.setOnClickListener(clickDigito);
        this.btn0.setOnClickListener(clickDigito);
        this.btnPonto.setOnClickListener(clickDigito);


        this.btnLimpar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display.limparDisplay();
            }
        });


        this.btnSoma.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {

                operacao = ADICAO ;
                calcular() ;
            }
        });

        this.btnSubtracao.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {

                operacao = SUBTRACAO ;
                calcular() ;
            }
        });


        this.btnMultiplicacao.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {

                operacao = MULTIPLICACAO ;
                calcular() ;
            }
        });


        this.btnDivisao.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {

                operacao = DIVISAO ;
                calcular() ;
            }
        });

        this.btnIgual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              calcular();


            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();

    }



    private void calcular(){

    if(Double.isNaN(operando1)){
        this.operando1 = display.getValue() ;
        this.display.setLimpaDisplay(true);
    }else{

        operando2 = display.getValue() ;
        this.display.setLimpaDisplay(true);


        switch (operacao){
            case ADICAO : operando1 = operando1 + operando2 ; break;
            case SUBTRACAO : operando1 = operando1 - operando2 ; break;
            case MULTIPLICACAO: operando1 = operando1 * operando2 ; break;
            case DIVISAO: operando1 = operando1 / operando2 ; break;

        }

        display.setText(operando1);

    }



    }




























}
