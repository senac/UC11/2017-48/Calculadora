package br.com.senac.calculadora;


import android.view.View;
import android.widget.Button;

/**
 * Created by sala302b on 30/01/2018.
 */

public class ClickDigito implements View.OnClickListener {

    private Display display ;

    public ClickDigito(Display display){
        this.display = display;
    }


    @Override
    public void onClick(View v) {
        String texto = this.display.getText() ;
        Button button = (Button)v ;
        String digito = button.getText().toString() ;

        if(display.isLimpaDisplay() &&  !digito.equals(".")){
            this.display.setText(digito);
            this.display.setLimpaDisplay(false);
        }else{

                if(button.getId() != R.id.btnPonto){
                    this.display.setText(texto + digito);
                }else{
                    if(!texto.contains(".")){
                        this.display.setText(texto + digito);
                    }
            }



        }



    }














}
