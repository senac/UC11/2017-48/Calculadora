package br.com.senac.calculadora;


import android.widget.TextView;

public class Display {

    private TextView textView ;
    private boolean limpaDisplay = true ;


   public Display(TextView textView){
        this.textView = textView ;
    }

   public TextView getTextView(){
        return this.textView ;
   }

   public boolean isLimpaDisplay(){
       return this.limpaDisplay ;
   }

   public void setLimpaDisplay(boolean limpaDisplay){
       this.limpaDisplay = limpaDisplay ;
   }

   public void setText(String valor){
       this.textView.setText(valor);
   }

   public void setText(double valor){
       this.textView.setText(String.valueOf(valor));
   }

   public String getText(){
       return this.textView.getText().toString() ;
   }

   public double getValue(){
       return  Double.parseDouble(this.textView.getText().toString());
   }

   public void limparDisplay(){
       this.setLimpaDisplay(true);
       this.setText(0);
   }















}
